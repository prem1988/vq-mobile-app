// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
    swipeBackPage: false,
    swipeBackPageThreshold: 1,
    swipePanel: "left",
    swipePanelCloseOpposite: true,
    pushState: true,
    pushStateRoot: undefined,
    pushStateNoAnimation: false,
    pushStateSeparator: '#!/',
    template7Pages: true
});


// Export selectors engine
var $$ = Dom7;

//Index page
$$(document).on('pageInit', '.page[data-page="index"]', function (e) {
    //Chat Pate

    $("#companylogo").attr("src", window.localStorage.getItem("VQ_LOGO"));
  
    

});

//Top Menu
$$(document).on('pageInit', '.page[data-page="topmenu"]', function (e) {
     //Chat Pate
    $$('.chatbut').on('click', function () {
        webpageView('chat');
   });
    //Tradies
    $$('.tradies').on('click', function () {
        webpageView('dir');

    });

    $$('.joblist').on('click', function () {
        webpageView('main');

    });

    $$('.postjobs').on('click', function () {
        webpageView('sub');

    });
    $$('.profile').on('click', function () {
        webpageView('profile');

    });
    $$('.manage').on('click', function () {
        webpageView('manage');

    });
    $$('.editprofile').on('click', function () {
        webpageView('editprofile');

    });
    $$('.faq').on('click', function () {
        webpageView('faq');

    });
    });


$$(document).on('pageBeforeInit', '.page[data-page="topmenu"]', function (e) {

    
    $('#mymenu').attr("href", "index.html");
});

$$(document).on('pageBeforeInit', '.page[data-page="index"]', function (e) {

    
    $('#mymenu').attr("href", "menu.html");
});

//----- Share Page -------//
// Option 2. Using live 'pageInit' event handlers for each page (not recommended)
$$(document).on('pageInit', '.page[data-page="vqshare"]', function (e) {
    
   
   
    //---Text box Loding -------//
    if (!(window.localStorage.getItem("VQ_DB_urlusername"))) {
        //Set URL

        $('#box').html(" Hi, check out the VisualQuotes app here, www.visualquotes.co.nz, it helps me find tradies to quote on my jobs without meetings. What a time saver! ");
        message = "www.visualquotes.co.nz";
    } else {
        $('#box').html(" Hi, check out the <a href=www." + window.localStorage.getItem("VQ_DB_urlusername") + ".visualquotes.co.nz> VisualQuotes app here, </a>, it helps me find tradies to quote on my jobs without meetings. What a time saver! www." + window.localStorage.getItem("VQ_DB_urlusername") + ".visualquotes.co.nz");
     //   $('#box').val("Hi, check out the VisualQuotes app here, www." + window.localStorage.getItem("VQ_DB_urlusername") + "visualquotes.co.nz,it helps me find tradies to quote on my jobs without meetings. What a time saver!")
        message = "www." + window.localStorage.getItem("VQ_DB_urlusername") + ".visualquotes.co.nz";
    }
    //Setting message
    $('#sendtype').change(function () {
        var selectedoption = $(this).find('option:selected');
        if (selectedoption.val() == "contractor") {
            //contractor message"
             $('#box').html("Hi, check out mybusiness <a href='www.'" + window.localStorage.getItem("VQ_DB_urlusername") + "'visualquotes.co.nz'> here</a></b>, VisualQuotes helps me to find jobs and quote without meetings, improving my productivity.");
           // $('#box').val("Hi, check out my business here  www.visualquotes.co.nz . VisualQuotes helps me to find jobs and quote without meetings, improving my productivity. ");
        } else {
            //Consumer message
            //record-job/c/59810

             $('#box').html(" Hi, Here is the link to <a href='www.'" + window.localStorage.getItem("VQ_DB_urlusername") + "'visualquotes.co.nz/'> my business</a>, or <a href='www.visualquotes.co.nz/record-job/c/59810'> Click here </a>to send your job detail and I will get back to you with a quote ASAP");
           // $('#box').val("Hi, Here is the link to my business www." + window.localStorage.getItem("VQ_DB_urlusername") + "visualquotes.co.nz, or send your job deail info here, wwww.visualquotes.co.nz/record-job/c/59810 and I will get back to you with a quote ASAP")
        }
    });
  


    // Do something here when page with data-page="about" attribute loaded and initialized
    
    $$('.smsbut').on('click', function () {
       

       
        var msgvalue = $('#box').val();
        window.plugins.socialsharing.share(msgvalue, 'VisualQuotes');
    });

})
// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: false
});

//
function webpageView(page) {
    
       
        user = window.localStorage.getItem("VQ_USERNAME");
        pass = window.localStorage.getItem("VQ_PASS");
       
        var page = page;

        //  var page = "main";
        var ref2 = cordova.ThemeableBrowser.open('http://www.visualquotes.co.nz/ajaxlogin1/' + user + '/' + pass + '/' + page + '', '_blank', {
            statusbar: {
                color: '#ffffffff'
            },
            toolbar: {
                height: 44,
                color: '#f0f0f0ff'
            },
            title: {
                color: '#003264ff',
                showPageTitle: true
            },
            backButton: {
                image: 'back',
                imagePressed: 'back_pressed',
                align: 'left',
                event: 'backPressed'
            },
            forwardButton: {
                image: 'forward',
                imagePressed: 'forward_pressed',
                align: 'left',
                event: 'forwardPressed'
            },
            closeButton: {
                image: 'close',
                imagePressed: 'close_pressed',
                align: 'left',
                event: 'closePressed'
            },
            customButtons: [
                {
                    image: 'share',
                    imagePressed: 'share_pressed',
                    align: 'right',
                    event: 'sharePressed'
                }
            ],
            menu: {
                image: 'menu',
                imagePressed: 'menu_pressed',
                title: 'Test',
                cancel: 'Cancel',
                align: 'right',
                items: [
                    {
                        event: 'helloPressed',
                        label: 'Hello World!'
                    },
                    {
                        event: 'testPressed',
                        label: 'Test!'
                    }
                ]
            },
            backButtonCanClose: true
        }).addEventListener('backPressed', function (e) {

        }).addEventListener('helloPressed', function (e) {

        }).addEventListener('sharePressed', function (e) {

        }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function (e) {
            console.error(e.message);
        }).addEventListener(cordova.ThemeableBrowser.EVT_WRN, function (e) {
            console.log(e.message);
        });

        ref2.addEventListener('loadstop', function (event) {

            ref2.executeScript({

                // code: "var img=document.querySelector('#logo img'); img.src='http://cordova.apache.org/images/cordova_bot.png';"
                code: "var img=document.querySelector('#header'); img.style.display='none';"
            })
        });
    

}


$$(document).on('pageInit', function (e) {
    $(".swipebox").swipebox();

    $("#ContactForm").validate({
        submitHandler: function (form) {
            ajaxContact(form);
            return false;
        }
    });

    $("#RegisterForm").validate();
    $("#LoginForm").validate();
    $("#ForgotForm").validate();

    $('a.backbutton').click(function () {
        parent.history.back();
        return false;
    });


    $(".posts li").hide();
    size_li = $(".posts li").size();
    x = 4;
    $('.posts li:lt(' + x + ')').show();
    $('#loadMore').click(function () {
        x = (x + 1 <= size_li) ? x + 1 : size_li;
        $('.posts li:lt(' + x + ')').show();
        if (x == size_li) {
            $('#loadMore').hide();
            $('#showLess').show();
        }
    });


    $("a.switcher").bind("click", function (e) {
        e.preventDefault();

        var theid = $(this).attr("id");
        var theproducts = $("ul#photoslist");
        var classNames = $(this).attr('class').split(' ');


        if ($(this).hasClass("active")) {
            // if currently clicked button has the active class
            // then we do nothing!
            return false;
        } else {
            // otherwise we are clicking on the inactive button
            // and in the process of switching views!

            if (theid == "view13") {
                $(this).addClass("active");
                $("#view11").removeClass("active");
                $("#view11").children("img").attr("src", "images/switch_11.png");

                $("#view12").removeClass("active");
                $("#view12").children("img").attr("src", "images/switch_12.png");

                var theimg = $(this).children("img");
                theimg.attr("src", "images/switch_13_active.png");

                // remove the list class and change to grid
                theproducts.removeClass("photo_gallery_11");
                theproducts.removeClass("photo_gallery_12");
                theproducts.addClass("photo_gallery_13");

            }

            else if (theid == "view12") {
                $(this).addClass("active");
                $("#view11").removeClass("active");
                $("#view11").children("img").attr("src", "images/switch_11.png");

                $("#view13").removeClass("active");
                $("#view13").children("img").attr("src", "images/switch_13.png");

                var theimg = $(this).children("img");
                theimg.attr("src", "images/switch_12_active.png");

                // remove the list class and change to grid
                theproducts.removeClass("photo_gallery_11");
                theproducts.removeClass("photo_gallery_13");
                theproducts.addClass("photo_gallery_12");

            }
            else if (theid == "view11") {
                $("#view12").removeClass("active");
                $("#view12").children("img").attr("src", "images/switch_12.png");

                $("#view13").removeClass("active");
                $("#view13").children("img").attr("src", "images/switch_13.png");

                var theimg = $(this).children("img");
                theimg.attr("src", "images/switch_11_active.png");

                // remove the list class and change to grid
                theproducts.removeClass("photo_gallery_12");
                theproducts.removeClass("photo_gallery_13");
                theproducts.addClass("photo_gallery_11");

            }

        }

    });

    document.addEventListener('touchmove', function (event) {
        if (event.target.parentNode.className.indexOf('navbarpages') != -1 || event.target.className.indexOf('navbarpages') != -1) {
            event.preventDefault();
        }
    }, false);

    // Add ScrollFix
    var scrollingContent = document.getElementById("pages_maincontent");
    new ScrollFix(scrollingContent);


    var ScrollFix = function (elem) {
        // Variables to track inputs
        var startY = startTopScroll = deltaY = undefined,

		elem = elem || elem.querySelector(elem);

        // If there is no element, then do nothing	
        if (!elem)
            return;

        // Handle the start of interactions
        elem.addEventListener('touchstart', function (event) {
            startY = event.touches[0].pageY;
            startTopScroll = elem.scrollTop;

            if (startTopScroll <= 0)
                elem.scrollTop = 1;

            if (startTopScroll + elem.offsetHeight >= elem.scrollHeight)
                elem.scrollTop = elem.scrollHeight - elem.offsetHeight - 1;
        }, false);
    };

    //Share buttion click


    
   

})
