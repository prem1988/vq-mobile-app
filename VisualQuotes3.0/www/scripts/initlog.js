﻿$(document).ready(function () {
    loadLoading();
    var username = "";
    var password = "";
    if (!(window.localStorage.getItem("VQ_USERNAME1")) && (!(window.localStorage.getItem("VQ_PASS1")))) {
        // if there is no login information
        //window.alert("no login");
        $("#loginbody").show();
        document.getElementById("loginbody").style.visibility = "visible";
        hideLoading();

    } else {
        // Authentical Already available

        // window.alert("ready");
        window.location.replace("profile.html");
        //$.mobile.pageContainer.pagecontainer("change", "index1.html", {transition: "slide"});
        //$.mobile.changePage("index1.html");

    }
    //Submit button validation
    $("#enter").click(function () {
        //  window.alert("it is now clicked");

        username = (document.getElementById("username").value);
        username = username.trim();
        password = (document.getElementById("password").value);
        password = password.trim();



        //username validation
        if ((validateEmail(username))) {
            //valid email
            //Check username
            if (password == "") {
                showDialog(" Please Enter the password", "Invalid Login");
                //$( "#passwordpopupBlank" ).popup( "open");
            } else {

                if ((validPassword(password))) {
                    //Authentication Successful
                    //loadLoading();
                    window.plugins.spinnerDialog.show(null, "Loading", true);
                    (authentication(username, password));
                    //  hideLoading();
                    window.plugins.spinnerDialog.hide();
                } else {
                    //Invalid password
                    showDialog(" Please Enter the valid password", "Invalid Login");
                    //  $( "#passwordValid" ).popup( "open" );
                }
            }


        } else {
            //Wrong email
            showDialog("Please Enter the valid Email", "Invalid Login");
            //  $( "#transitionExample" ).popup( "open" );
        }

    });

    //Show Dialog Box
    function showDialog(message, title) {
        navigator.notification.alert(
       message,  // message
       null,     // callbackq
       title,    // title
       'OK'      // buttonName
       );

    }

});