/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Loadin widget code

function loadLoading() {

}

function hideLoading() {

}

//Validate username/Email
function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    // return re.test(email);
    return true;
}

//Validate Password
function validPassword(pass) {
    re = /^\w+$/i;
    return re.test(pass);

}

//Jsonp call for validation
function authentication(username, pass) {

    var userval = username;
    var passval = pass;
    loadLoading();
    
  //  document.getElementById("serverload").style.visibility = "visible";
    window.plugins.spinnerDialog.show(null, "Loading", true);
    $.ajax({
         url: 'http://www.visualquotes.co.nz/mobtest1',
        //url: 'http://localhost:8080/mobtest1',
        data: { user: userval, pass: passval },
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'jsonpCallback',
        success: function () {
            //alert("success");
        }
    });
}

function jsonpCallback(data) {

   
   
    // document.getElementById("serverload").style.visibility = "hidden";
    window.plugins.spinnerDialog.hide();
    
    //  console.log("our ratings" + data.profileFigure.raw);
    //  alert(data.dashboard.raw);
    // alert(data.dashboard.followers); 
    //  alert(data.dashboard.reviews);
    //  console.log(data.jwtid);
    //Checking the validation
    if (data.authstatus == "true") {
        //Successfule Auth
        $('#resultLog').text("Valid Login");


        //Save Data to local storage
        window.localStorage.setItem("VQ_PROFILENAME", data.userprofile);
        window.localStorage.setItem("VQ_IMGSRC", data.profileurl);
        window.localStorage.setItem("VQ_USERNAME", data.name);
        window.localStorage.setItem("VQ_PASS", data.password);
        window.localStorage.setItem("VQ_COMPANYNAME", data.companyname);
        window.localStorage.setItem("VQ_LOGO", data.compic);

        window.localStorage.setItem("VQ_DB_rating", data.dashboard.raw);
        window.localStorage.setItem("VQ_DB_follower", data.dashboard.followers);
        window.localStorage.setItem("VQ_DB_reviews", data.dashboard.reviews);
        window.localStorage.setItem("VQ_DB_openjobs","0" );
        window.localStorage.setItem("VQ_DB_closejobs", "0");
        window.localStorage.setItem("VQ_DB_urlusername", data.username);

        window.location.replace('index.html');


        //    window.localStorage.setItem("VQ_JWT",data.jwtid);

        //direct to admin page
      //  var myinitPage = new Framework7();
        //var authview = myinitPage.addView('.view-main');
        //authview.router.loadPage('profile.html');

    } else if (data.authstatus == "false") {

        //Invalid Auth
       // $('#resultLog').text("Invalid Login");
        //$("#popupInvalid").popup("open");
        showDialog("Given Login Information is not matching ", "Invalid Login");
        //Show Dialog Box
        function showDialog(message, title) {
            navigator.notification.alert(
           message,  // message
           null,     // callbackq
           title,    // title
           'OK'      // buttonName
           );

        }

    } else {
        window.alert("issue");
    }


    // $('#resultLog').text(data.status);
}

function removeStorage1() {
    window.plugins.spinnerDialog.show(null, "Signing Out", true);
    localStorage.clear();
    window.plugins.spinnerDialog.hide();

    //redirct to main login page
    $("#t1").click();
    //window.location.replace("index.html");
}


//Contact us data 
function contactData(name, email, subject, message) {
        
    var nameval = name;
    var emailval = email;
    var subjectval = subject;
    var messageval = message;
    window.alert(messageval);

    $.ajax({


        url: 'http://www.visualquotes.co.nz/mobContact',
        //url: 'http://localhost:8080/mobContact',
        data: { name: nameval, email: emailval, subject: subjectval, message: messageval},
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'jsonpCallbackContact',
        success: function () {
            //  alert("success");
        }
    });
}

function jsonpCallbackContact(data) {
    window.alert("json check");
}
//  User Register Form

function userRegistration(name, companyName, email, pass, passconf) {


    var nameVal = name;
    var companyVal = companyName;
    var emailVal = email;
    var passVal = pass;
    var passconfVal = passconf;
    document.getElementById("regload").style.visibility = "visible";

    $.ajax({


         url: 'http://www.visualquotes.co.nz/moblRegister',
        //url: 'http://localhost:8080/moblRegister',
        data: { email: emailVal, name: nameVal, companyname: companyVal, password: passVal, password_confirmation: passconfVal },
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: 'jsonpCallbackReg',
        success: function () {
            //  alert("success");
        }
    });
}
function jsonpCallbackReg(data) {
    // alert("Inside");
    document.getElementById("regload").style.visibility = "hidden";
    //   alert(data.authstatus);
    if (data.regstatus == "saved") {
        //  window.alert("saved");
        // $("#reghead").text("SuccessFul");
        showDialog("Registration is successful", "Valid Signup");
        //  $("#regmessg").text(data.message);
        showDialog(data.message, "Success");
    } else if (data.regstatus == "notsaved") {
        //  window.alert("not saved");
        //$("#reghead").text("Error Message");
        showDialog(data.message, "Failed Signup");
    } else {

    }
   // $("#popupRegInvalid").popup("open");
    showDialog("Please Enter the Password", "Invalid Signup");
}

//Registraiton buttoin
$("#regbutton").click(function () {
    window.alert("register work");
    var name = $("#name").val();
    var companyName = $("#companyname").val();
    var email = $("#email").val();
    var pass = $("#password").val();
    var passconf = $("#password_confirmation").val();
    userRegistration(name, companyName, email, pass, passconf);

});